package com.onefootball.app.di

import com.onefootball.mainscreen.di.MainComponent
import com.onefootball.news.di.NewsComponent
import dagger.Component

@ApplicationScope
@Component(modules = [AppModule::class, DomainModule::class])
interface AppComponent {
    fun getMainComponent(): MainComponent
    fun getNewsComponent(): NewsComponent
}