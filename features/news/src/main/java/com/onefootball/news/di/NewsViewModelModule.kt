package com.onefootball.news.di

import androidx.lifecycle.ViewModel
import com.onefootball.common.di.ViewModelKey
import com.onefootball.news.NewsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface NewsViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(NewsViewModel::class)
    @NewsScope
    fun bindMainViewModel(viewModel: NewsViewModel): ViewModel
}