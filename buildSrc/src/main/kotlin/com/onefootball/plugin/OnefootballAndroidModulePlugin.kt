package com.onefootball.plugin

import Config.libraryPlugin
import org.gradle.api.Project

class OnefootballAndroidModulePlugin : BaseAndroidPlugin() {
    override fun apply(target: Project) {
        target.plugins.apply(libraryPlugin)
        super.apply(target)
    }
}