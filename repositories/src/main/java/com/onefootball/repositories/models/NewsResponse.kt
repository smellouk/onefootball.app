package com.onefootball.repositories.models

import com.google.gson.annotations.SerializedName

class NewsResponse(
    @SerializedName("news")
    var newsEntityList: List<NewsDto>? = null
)