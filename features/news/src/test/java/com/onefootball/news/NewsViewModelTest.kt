package com.onefootball.news

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.onefootball.news.domain.GetNewsDataState
import com.onefootball.news.domain.GetNewsUseCase
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test

class NewsViewModelTest : BaseTest() {
    @Rule
    @JvmField
    var testSchedulerRule = RxImmediateSchedulerRule()

    @Rule
    @JvmField
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @RelaxedMockK
    lateinit var getNewsUseCase: GetNewsUseCase

    @RelaxedMockK
    lateinit var viewStateMapper: ViewStateMapper

    @InjectMockKs
    lateinit var viewModel: NewsViewModel

    @Test
    fun getInitialState_ShouldBeInitial() {
        assertEquals(
            "getInitialState should be Initial",
            ViewState.Initial,
            viewModel.getInitialState()
        )
    }

    @Test
    fun onGetNewsCommand_shouldGetNews() {
        every {
            getNewsUseCase.buildObservable()
        } returns Observable.just(
            GetNewsDataState.Successful(
                listOf(givenEmptyNews)
            )
        )

        viewModel.onCommand(Command.GetNews)

        verify {
            getNewsUseCase.buildObservable()
            viewStateMapper.map(any())
        }
    }
}