package com.onefootball.news

import com.onefootball.news.domain.GetNewsDataState
import com.onefootball.news.domain.GetNewsUseCase
import com.onefootball.news.domain.NewsMapper
import com.onefootball.repositories.news.NewsRepository
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Single
import org.junit.Test

class GetNewsUseCaseTest : BaseTest() {
    @RelaxedMockK
    lateinit var newsRepository: NewsRepository

    @RelaxedMockK
    lateinit var newsMapper: NewsMapper

    @InjectMockKs
    lateinit var getNewsUseCase: GetNewsUseCase

    @Test
    fun getNews_ShouldEmitSuccessfulDataState() {
        every {
            newsRepository.getNews()
        } returns Single.just(givenResponse)

        every {
            newsMapper.map(givenResponse)
        } returns newsList

        getNewsUseCase.buildObservable()
            .test()
            .run {
                assertComplete()
                assertNoErrors()
                assertValueCount(1)
                assertValue { dataState ->
                    dataState is GetNewsDataState.Successful
                }
            }
    }

    @Test
    fun getNews_ShouldEmitFailDataState() {
        every {
            newsRepository.getNews()
        } returns Single.just(givenEmptyResponse)

        every {
            newsMapper.map(givenEmptyResponse)
        } returns emptyNewsList

        getNewsUseCase.buildObservable()
            .test()
            .run {
                assertComplete()
                assertNoErrors()
                assertValueCount(1)
                assertValue { dataState ->
                    dataState is GetNewsDataState.Fail
                }
            }
    }
}