package com.onefootball.news

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.onefootball.common.models.News
import kotlinx.android.synthetic.main.news_item.view.*

class NewsAdapter(
    private val onItemClick: (News) -> Unit
) : RecyclerView.Adapter<NewsViewHolder>() {
    private val news = mutableListOf<News>()

    fun setNews(list: List<News>) {
        news.clear()
        news.addAll(list)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = NewsViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.news_item, parent, false
        ).apply {
            setOnClickListener {
                onItemClick((tag as News))
            }
        }
    )

    override fun getItemCount(): Int = news.size

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val news = news[position]
        with(holder.itemView) {
            tag = news
            tvTitle.text = news.title
            ivNews.load(url = news.imageUrl)
            ivResource.load(url = news.resourceUrl)
            tvResourceName.text = news.resourceName
        }
    }
}

class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)