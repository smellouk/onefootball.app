package com.onefootball.repositories

import com.google.gson.Gson
import com.onefootball.repositories.news.NewsRepository
import com.onefootball.repositories.news.NewsRepositoryImpl
import org.junit.Test
import java.io.BufferedReader
import java.io.InputStreamReader

class NewsRepositoryImplTest {
    private val gson = Gson()
    private val newsRepository: NewsRepository

    init {
        newsRepository = NewsRepositoryImpl(
            BufferedReader(
                InputStreamReader(
                    javaClass.getResourceAsStream("news.json"),
                    "UTF-8"
                )
            ),
            gson
        )
    }

    @Test
    fun getNews_ShouldReturnResponseWithData() {
        newsRepository.getNews()
            .test()
            .apply {
                assertComplete()
                assertNoErrors()
                assertValueCount(1)
                assertValue { response ->
                    response.newsEntityList?.let { news ->
                        news.isNotEmpty() && news.size == 2
                    } ?: false
                }
            }
    }
}