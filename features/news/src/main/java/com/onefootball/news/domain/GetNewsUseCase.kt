package com.onefootball.news.domain

import com.onefootball.common.domain.BaseUseCaseNonParams
import com.onefootball.news.di.NewsScope
import com.onefootball.repositories.news.NewsRepository
import io.reactivex.Observable
import javax.inject.Inject

@NewsScope
class GetNewsUseCase @Inject constructor(
    private val repository: NewsRepository,
    private val mapper: NewsMapper
) : BaseUseCaseNonParams<GetNewsDataState> {
    override fun buildObservable(): Observable<GetNewsDataState> = repository.getNews()
        .map { response ->
            mapper.map(response).let { news ->
                if (news.isEmpty()) {
                    GetNewsDataState.Fail
                } else {
                    GetNewsDataState.Successful(news)
                }
            }
        }.toObservable()
}