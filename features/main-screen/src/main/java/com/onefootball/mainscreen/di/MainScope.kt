package com.onefootball.mainscreen.di

import javax.inject.Scope

@Scope
@Retention
annotation class MainScope