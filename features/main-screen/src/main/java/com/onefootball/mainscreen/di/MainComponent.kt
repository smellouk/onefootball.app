package com.onefootball.mainscreen.di

import com.onefootball.common.base.BaseComponentProvider
import com.onefootball.mainscreen.MainScreenActivity
import dagger.Subcomponent

@MainScope
@Subcomponent(modules = [MainViewModelModule::class])
interface MainComponent {
    fun inject(mainScreenActivity: MainScreenActivity)
}

interface MainComponentProvider : BaseComponentProvider {
    fun getMainComponent(): MainComponent
}