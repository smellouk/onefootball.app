package com.onefootball.app.di

import android.content.Context
import com.onefootball.app.R
import dagger.Module
import dagger.Provides
import java.io.BufferedReader

@Module
class AppModule(private val context: Context) {
    @ApplicationScope
    @Provides
    fun provideContext() = context

    @ApplicationScope
    @Provides
    fun provideNewsRaw(context: Context): BufferedReader = context.resources
        .openRawResource(R.raw.news)
        .bufferedReader()
}