package com.onefootball.news.domain

import com.onefootball.common.domain.BaseDataState
import com.onefootball.common.models.News

sealed class GetNewsDataState : BaseDataState {
    class Successful(val data: List<News>) : GetNewsDataState()
    object Fail : GetNewsDataState() {
        val message = "Something is wrong with the source of your data!"
    }
}