package com.onefootball.mainscreen

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.onefootball.common.base.BaseActivity
import com.onefootball.mainscreen.ViewState.Initial
import com.onefootball.mainscreen.ViewState.NewsScreen
import com.onefootball.mainscreen.di.MainComponentProvider

class MainScreenActivity : BaseActivity<MainComponentProvider, ViewState, MainViewModel>(
    R.layout.activity_main_screen
) {
    private val navController: NavController by lazy {
        Navigation.findNavController(
            this, R.id.navMain
        )
    }

    override fun getViewModelClass(): Class<out MainViewModel> = MainViewModel::class.java

    override fun inject() {
        componentProvider.getMainComponent().inject(this)
    }

    override fun renderViewState(state: ViewState) {
        when (state) {
            is Initial -> renderDefaultViewState()
            is NewsScreen -> navigateTo(state.destination)
        }
    }

    private fun navigateTo(@IdRes destination: Int, bundle: Bundle? = null) {
        navController.navigate(destination, bundle)
    }
}