package com.onefootball.common.models

data class News(
    val title: String,
    val resourceName: String,
    val resourceUrl: String,
    val imageUrl: String,
    val newsLink: String
)

