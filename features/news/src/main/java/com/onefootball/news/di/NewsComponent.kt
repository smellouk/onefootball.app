package com.onefootball.news.di

import com.onefootball.common.base.BaseComponentProvider
import com.onefootball.news.NewsFragment
import dagger.Subcomponent

@NewsScope
@Subcomponent(modules = [NewsViewModelModule::class])
interface NewsComponent {
    fun inject(newsFragment: NewsFragment)
}

interface NewsComponentProvider : BaseComponentProvider {
    fun getNewsComponent(): NewsComponent
}