package com.onefootball.news

import com.onefootball.common.base.BaseViewState
import com.onefootball.common.models.News

sealed class ViewState : BaseViewState {
    object Initial : ViewState()
    object Pending : ViewState()
    class NewsReady(val news: List<News>) : ViewState()
    class Error(val message: String) : ViewState()
}