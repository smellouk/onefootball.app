package com.onefootball.news

import com.onefootball.news.domain.NewsMapper
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class NewsMapperTest {
    private val newsMapper = NewsMapper()

    @Test
    fun mapNonEmptyResponse_ShouldMapToListOfNews() {
        newsMapper.map(givenResponse).run {
            assertTrue("for this news list case it should be not empty!", isNotEmpty())
        }
    }

    @Test
    fun mapEmptyResponse_ShouldMapToListOfNews() {
        newsMapper.map(givenEmptyResponse).run {
            assertTrue("for this news list case it should empty!", isEmpty())
        }
    }

    @Test
    fun mapEmptyDto_ShouldMapToEmptyNews() {
        newsMapper.map(givenEmptyNewsDto).run {
            assertEquals("", givenEmptyNews, this)
        }
    }
}