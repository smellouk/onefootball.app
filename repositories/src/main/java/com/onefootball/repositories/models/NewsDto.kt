package com.onefootball.repositories.models

import com.google.gson.annotations.SerializedName

class NewsDto(
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("resource_name")
    val resourceName: String? = null,
    @SerializedName("resource_url")
    val resourceUrl: String? = null,
    @SerializedName("image_url")
    val imageUrl: String? = null,
    @SerializedName("news_link")
    val newsLink: String? = null
)

