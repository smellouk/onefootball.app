package com.onefootball.repositories.di

import com.google.gson.Gson
import com.onefootball.repositories.news.NewsRepository
import com.onefootball.repositories.news.NewsRepositoryImpl
import dagger.Module
import dagger.Provides
import java.io.BufferedReader
import javax.inject.Singleton

@Module
class RepositoryModule(private val newsSource: BufferedReader) {
    @Singleton
    @Provides
    fun providerNewsSource() = newsSource

    @Singleton
    @Provides
    fun provideGson() = Gson()

    @Singleton
    @Provides
    fun provideNewsRepository(
        newsSource: BufferedReader,
        gson: Gson
    ): NewsRepository = NewsRepositoryImpl(newsSource, gson)
}