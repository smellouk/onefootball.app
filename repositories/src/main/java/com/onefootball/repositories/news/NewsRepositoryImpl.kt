package com.onefootball.repositories.news

import com.google.gson.Gson
import com.onefootball.repositories.models.NewsResponse
import io.reactivex.Single
import java.io.BufferedReader

class NewsRepositoryImpl(
    private val newsSource: BufferedReader,
    private val gson: Gson
) : NewsRepository {
    override fun getNews(): Single<NewsResponse> = Single.just(
        gson.fromJson(newsSource.readText(), NewsResponse::class.java)
    )
}