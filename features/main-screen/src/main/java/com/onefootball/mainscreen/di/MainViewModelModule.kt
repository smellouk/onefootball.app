package com.onefootball.mainscreen.di

import androidx.lifecycle.ViewModel
import com.onefootball.common.di.ViewModelKey
import com.onefootball.mainscreen.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface MainViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    @MainScope
    fun bindMainViewModel(viewModel: MainViewModel): ViewModel
}