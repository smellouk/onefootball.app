package com.onefootball.mainscreen

import androidx.annotation.IdRes
import com.onefootball.common.base.BaseViewState

sealed class ViewState : BaseViewState {
    object Initial : ViewState()
    object NewsScreen : ViewState() {
        @IdRes
        val destination: Int = R.id.show_news_list
    }
}