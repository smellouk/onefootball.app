package com.onefootball.news.domain

import com.onefootball.common.models.News
import com.onefootball.news.di.NewsScope
import com.onefootball.repositories.models.NewsDto
import com.onefootball.repositories.models.NewsResponse
import javax.inject.Inject

@NewsScope
class NewsMapper @Inject constructor() {
    fun map(dto: NewsDto) = with(dto) {
        News(
            title = title ?: "",
            resourceName = resourceName ?: "",
            resourceUrl = resourceUrl ?: "",
            imageUrl = imageUrl ?: "",
            newsLink = newsLink ?: ""
        )
    }

    fun map(newsResponse: NewsResponse) = newsResponse.newsEntityList?.map { dto ->
        map(dto)
    } ?: emptyList()
}