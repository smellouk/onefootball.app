object Config {
    const val compileSdkVersion = 29
    const val buildToolsVersion = "29.0.3"
    const val appId = "com.onefootball.app"
    const val mindSdkVersion = 16 //more users
    const val targetSdkVersion = 29
    const val javaVersion = "1.8"
    const val applicationPlugin = "com.android.application"
    const val libraryPlugin = "com.android.library"
    const val kotlinAndroidPlugin = "kotlin-android"
    const val kotlinAndroidExtPlugin = "kotlin-android-extensions"
    const val kotlinKaptPlugin = "kotlin-kapt"
    const val kotlinPlugin = "kotlin"
}