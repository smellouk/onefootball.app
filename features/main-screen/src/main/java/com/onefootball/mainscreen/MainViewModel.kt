package com.onefootball.mainscreen

import com.onefootball.common.base.BaseViewModel
import com.onefootball.mainscreen.ViewState.Initial
import com.onefootball.mainscreen.di.MainScope
import javax.inject.Inject

@MainScope
class MainViewModel @Inject constructor() : BaseViewModel<ViewState>() {
    override fun getInitialState() = Initial
}