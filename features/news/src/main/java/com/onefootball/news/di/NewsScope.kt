package com.onefootball.news.di

import javax.inject.Scope

@Scope
@Retention
annotation class NewsScope