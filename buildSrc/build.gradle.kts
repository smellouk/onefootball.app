plugins {
    `kotlin-dsl`
}

object PluginVersion {
    const val android = "4.0.0"
    const val kotlin = "1.3.72"
}

repositories {
    mavenCentral()
    google()
    jcenter()
}

dependencies {
    implementation("com.android.tools.build:gradle:${PluginVersion.android}")
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:${PluginVersion.kotlin}")
}

gradlePlugin {
    plugins {
        register("OnefootballAppPlugin") {
            id = "onefootball-app"
            implementationClass = "com.onefootball.plugin.OnefootballAppPlugin"
        }
        register("OnefootballAndroidModulePlugin") {
            id = "onefootball-android-module"
            implementationClass = "com.onefootball.plugin.OnefootballAndroidModulePlugin"
        }
        register("OnefootballKotlinModulePlugin") {
            id = "onefootball-kotlin-module"
            implementationClass = "com.onefootball.plugin.OnefootballKotlinModulePlugin"
        }
    }
}