package com.onefootball.common.base

interface Commandable<Cmd : BaseCommand> {
    fun onCommand(cmd: Cmd)
}