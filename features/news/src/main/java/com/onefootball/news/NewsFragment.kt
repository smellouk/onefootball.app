package com.onefootball.news

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import com.onefootball.common.base.BaseFragment
import com.onefootball.common.exhaustive
import com.onefootball.common.hide
import com.onefootball.common.models.News
import com.onefootball.common.show
import com.onefootball.news.Command.GetNews
import com.onefootball.news.ViewState.*
import com.onefootball.news.di.NewsComponentProvider
import kotlinx.android.synthetic.main.error_view.*
import kotlinx.android.synthetic.main.fragment_news.*

class NewsFragment : BaseFragment<NewsComponentProvider, ViewState, NewsViewModel>(
    R.layout.fragment_news
) {
    private val onItemClick: (News) -> Unit = { news ->
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(news.newsLink)))
    }

    private val newsAdapter = NewsAdapter(onItemClick)

    override fun getViewModelClass(): Class<out NewsViewModel> = NewsViewModel::class.java

    override fun inject() {
        componentProvider.getNewsComponent().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvNews.adapter = newsAdapter
    }

    override fun renderViewState(state: ViewState) {
        when (state) {
            is Initial -> viewModel.onCommand(GetNews)
            is Pending -> renderDefaultViewState()
            is NewsReady -> renderNewsList(state)
            is Error -> renderError(state)
        }.exhaustive
    }

    private fun renderNewsList(state: NewsReady) {
        rvNews.show()
        tvError.hide()
        newsAdapter.setNews(state.news)
    }

    private fun renderError(errorViewState: Error) {
        rvNews.hide()
        tvError.show()
        tvError.text = errorViewState.message
    }
}