package com.onefootball.plugin

import Config.applicationPlugin
import org.gradle.api.Project

class OnefootballAppPlugin : BaseAndroidPlugin() {
    override fun apply(target: Project) {
        target.plugins.apply(applicationPlugin)
        super.apply(target)
    }
}