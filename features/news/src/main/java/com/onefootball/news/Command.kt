package com.onefootball.news

import com.onefootball.common.base.BaseCommand

sealed class Command : BaseCommand {
    object GetNews : Command()
}