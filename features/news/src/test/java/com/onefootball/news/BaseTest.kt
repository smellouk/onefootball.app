package com.onefootball.news

import com.onefootball.common.models.News
import com.onefootball.repositories.models.NewsDto
import com.onefootball.repositories.models.NewsResponse
import io.mockk.MockKAnnotations
import org.junit.Before

abstract class BaseTest {
    @Before
    fun setup() {
        MockKAnnotations.init(this)
    }
}

val givenEmptyNewsDto = NewsDto()
val givenResponse = NewsResponse(listOf(givenEmptyNewsDto))
val givenEmptyResponse = NewsResponse()
val givenEmptyNews = News("", "", "", "", "")
val newsList = listOf(givenEmptyNews)
val emptyNewsList = listOf<News>()