package com.onefootball.news

import com.onefootball.common.base.BaseViewModel
import com.onefootball.common.base.Commandable
import com.onefootball.common.exhaustive
import com.onefootball.news.di.NewsScope
import com.onefootball.news.domain.GetNewsUseCase
import javax.inject.Inject

@NewsScope
class NewsViewModel @Inject constructor(
    private val getNewsUseCase: GetNewsUseCase,
    private val viewStateMapper: ViewStateMapper
) : BaseViewModel<ViewState>(), Commandable<Command> {
    override fun getInitialState(): ViewState = ViewState.Initial

    override fun onCommand(cmd: Command) {
        liveData.value = when (cmd) {
            is Command.GetNews -> onGetNews()
        }.exhaustive
    }

    private fun onGetNews(): ViewState.Pending {
        addObservable(getNewsUseCase.buildObservable(),
            onNext = { dataState ->
                liveData.value = viewStateMapper.map(dataState)
            }, onError = { throwable ->
                liveData.value = ViewState.Error(throwable?.message ?: "")
            }
        )
        return ViewState.Pending
    }
}