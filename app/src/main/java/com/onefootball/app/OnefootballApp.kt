package com.onefootball.app

import android.app.Application
import com.onefootball.app.di.AppComponent
import com.onefootball.app.di.AppModule
import com.onefootball.app.di.DaggerAppComponent
import com.onefootball.mainscreen.di.MainComponent
import com.onefootball.mainscreen.di.MainComponentProvider
import com.onefootball.news.di.NewsComponent
import com.onefootball.news.di.NewsComponentProvider

class OnefootballApp : Application(), MainComponentProvider, NewsComponentProvider {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    override fun getMainComponent(): MainComponent = appComponent.getMainComponent()

    override fun getNewsComponent(): NewsComponent = appComponent.getNewsComponent()
}