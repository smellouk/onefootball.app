package com.onefootball.repositories.di

import com.onefootball.repositories.RepositoryProvider
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RepositoryModule::class])
interface RepositoryComponent {
    fun inject(provider: RepositoryProvider)
}