package com.onefootball.app.di

import com.onefootball.repositories.RepositoryProvider
import dagger.Module
import dagger.Provides
import java.io.BufferedReader

@Module
class DomainModule {
    @ApplicationScope
    @Provides
    fun provideRepositories(newsSource: BufferedReader) = RepositoryProvider(newsSource)

    @ApplicationScope
    @Provides
    fun provideNewsRepository(repositoryProvider: RepositoryProvider) =
        repositoryProvider.getNewsRepository()
}