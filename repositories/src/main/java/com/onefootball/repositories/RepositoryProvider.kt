package com.onefootball.repositories

import com.onefootball.repositories.di.DaggerRepositoryComponent
import com.onefootball.repositories.di.RepositoryModule
import com.onefootball.repositories.news.NewsRepository
import java.io.BufferedReader
import javax.inject.Inject

class RepositoryProvider(newsSource: BufferedReader) {
    @Inject
    internal lateinit var newsRepositoryIntern: NewsRepository

    init {
        DaggerRepositoryComponent.builder()
            .repositoryModule(RepositoryModule(newsSource))
            .build().apply {
                inject(this@RepositoryProvider)
            }
    }

    fun getNewsRepository() = newsRepositoryIntern
}