package com.onefootball.common.domain

import io.reactivex.Observable

interface BaseUseCase<Params : BaseParams, DataState : BaseDataState> {
    fun buildObservable(params: Params): Observable<DataState>
}

interface BaseUseCaseNonParams<DataState : BaseDataState> {
    fun buildObservable(): Observable<DataState>
}