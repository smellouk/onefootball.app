package com.onefootball.news

import com.onefootball.common.domain.BaseDataState
import com.onefootball.common.exhaustive
import com.onefootball.news.di.NewsScope
import com.onefootball.news.domain.GetNewsDataState
import javax.inject.Inject

@NewsScope
class ViewStateMapper @Inject constructor() {
    fun map(dataState: BaseDataState) = when (dataState) {
        is GetNewsDataState.Successful -> ViewState.NewsReady(dataState.data)
        is GetNewsDataState.Fail -> ViewState.Error(dataState.message)
        else -> ViewState.Pending
    }.exhaustive
}