package com.onefootball.repositories.news

import com.onefootball.repositories.models.NewsResponse
import io.reactivex.Single

interface NewsRepository {
    fun getNews(): Single<NewsResponse>
}