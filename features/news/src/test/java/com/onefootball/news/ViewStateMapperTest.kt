package com.onefootball.news

import com.onefootball.news.domain.GetNewsDataState
import org.junit.Assert.assertTrue
import org.junit.Test

class ViewStateMapperTest {
    private val viewStateMapper = ViewStateMapper()

    @Test
    fun mapSuccessfulDataState_ShouldMapToNewsReadyState() {
        viewStateMapper.map(givenSuccessfulDataState).run {
            assertTrue(
                "Expected view state is NewsReady",
                this is ViewState.NewsReady &&
                        this.news == givenListNews
            )
        }
    }
    
    @Test
    fun mapFailDataState_ShouldMapToNewsErrorState() {
        viewStateMapper.map(givenFailDataState).run {
            assertTrue(
                "Expected view state is Error",
                this is ViewState.Error &&
                        this.message == givenFailDataState.message
            )
        }
    }
}

private val givenListNews = listOf(givenEmptyNews)
private val givenSuccessfulDataState = GetNewsDataState.Successful(givenListNews)
private val givenFailDataState = GetNewsDataState.Fail