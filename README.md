# Onefootball Coding Challenge
![](https://media.giphy.com/media/uADx98ByhpOwcE7KhW/giphy.gif)

## Want to clone the project ?
* Gitlab: https://gitlab.com/smellouk/onefootball.app
* Github: Ask me to invite by sending me email to : s.mellouk@live.com

## Project Setup
* MVVM Clean architecture
* Dagger (Component, SubComponent with scoping)
* Multi modules
* Unit Tests with MockK
* buildSrc
* RxJava
* Navigation Component

## To Add
* Travis for building and Testing

## Demo
* Debug: Test [me](demo/app-debug.apk)
* Release : Test [me](demo/app-release.apk)
<img src="demo/screenshot1.png" width="400" />

## How to build release version?
1- add this to your local properties
```
storePassword=123456789
keyAlias=Onefootball
keyPassword=987654321
```
2- checkout `master` branch

3- run `./gradlew aR`

## How to check current version of the code
run `./gradlew pV`