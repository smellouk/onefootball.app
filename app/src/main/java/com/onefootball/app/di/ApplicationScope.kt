package com.onefootball.app.di

import javax.inject.Scope

@Scope
@Retention
annotation class ApplicationScope